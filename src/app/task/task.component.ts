import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent {
  @Input() task = '';
  @Output() delete = new EventEmitter();
  @Output() taskChange = new EventEmitter<string>();
  icon = 'https://icon-library.com/images/icon-delete/icon-delete-16.jpg';

  getTask() {
    return this.task;
  }

  onDeleteClick() {
    this.delete.emit();
  }

  onTaskChange(event: Event) {
    const target = <HTMLInputElement>event.target;
    this.taskChange.emit(target.value);
  }
}
