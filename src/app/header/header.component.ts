import {Component} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  task = '';
  tasks = [
    {task: 'Go to sleep'},
    {task: 'Check your mailbox'},
  ];

  onCreateTask(event: Event) {
    const target = <HTMLInputElement>event.target;
    this.task = target.value;
  }

  onAddTask(event: Event) {
    event.preventDefault();
    if (this.task !== '') {
      this.tasks.push({
        task: this.task,
      });
    } else {
      console.log('Error. Write a task to do.');
    }
  }

  onDeleteTask(i: number) {
    this.tasks.splice(i, 1);
  }

  changeTask(index: number, newTask: string) {
    this.tasks[index].task = newTask;
  }
}
